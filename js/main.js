var strHost = window.location.href;
if (strHost.indexOf("php") > 0) {
    urlTarget = urlTargetPhp;
} else if (strHost.indexOf("java") > 0) {
    urlTarget = urlTargetJava;
}

function AjaxCall(serverFile = null, functionBody = '', method = '', objBody = '', objTarget = '', objOrigin = '', async = true, timeout = '50000000') {
    document.getElementById("compStatus").innerHTML = strMsg["load"];
    if (serverFile == '' || serverFile == null) {
        return;
    }
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    document.getElementById("compStatus").style.display = "";
    xmlhttp.ontimeout = function() {
        document.getElementById("compStatus").innerHTML = strMsg["time"];
    };
    xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                strResponse = this.responseText;
                if (strResponse.indexOf("{") != null) {
                    strResponse = strResponse.substring(strResponse.indexOf("{"));
                }
                try {
                    objResponse = JSON.parse(strResponse);
                } catch {
                    objResponse = strResponse;
                }
                document.getElementById("compStatus").style.display = "none";
                if (functionBody != '') {
                    functionBody(objResponse, objTarget, objOrigin);
                } else {
                    if (objResponse['table'] != null) {
                        target = objResponse['table'] + 's';
                        action = 'list';
                        AjaxCall(urlTarget + "functions/" + target, fncConstruc, "GET", "", "list", action);
                    } else {
                        return objResponse;
                    }
                }
                return true;
            } else if (this.readyState == 0 || this.readyState == 1 || this.readyState == 3) {
                document.getElementById("compStatus").innerHTML = strMsg["load"];
            } else {
                //request failed.
                document.getElementById("compStatus").innerHTML = strMsg["out"];
            }
        }
        //Verificamos que el encabezado JWT no este presente
    xmlhttp.open(method, serverFile, async);
    var header = new Headers();
    //if (header.get("jwt")) {
    //    xmlhttp.setRequestHeader("jwt", header.get("jwt"));
    //}
    if (document.getElementById("jwt").value != "") {
        xmlhttp.setRequestHeader("jwt", document.getElementById("jwt").value);
    }
    if (objBody != '') {
        xmlhttp.setRequestHeader("Content-type", "application/json");
    }
    xmlhttp.timeout = timeout;
    xmlhttp.send(objBody);
}



function getText(readFile, objTarget) {
    var reader;
    try {
        reader = new FileReader();
    } catch (e) {
        document.getElementById('compStatus').innerHTML = "Error: esta API no esta soportada por su navegador";
        return;
    }

    // Read file into memory as UTF-8      
    reader.readAsText(readFile, "UTF-8");
    //reader.readAsDataURL(readFile);
    // Handle progress, success, and errors
    reader.onload = function(evt) {
        // Obtain the read file data    
        var fileString = evt.target.result;
        document.getElementById(objTarget).innerHTML = fileString;
    };
    reader.onerror = function(evt) {
        if (evt.target.error.code == evt.target.error.NOT_READABLE_ERR) {
            // The file could not be read
            document.getElementById('output').innerHTML = "Error reading file..."
        }
    };
}