const urlTargetPhp = "http://phphtmlproxy.local/";
const urlTargetJava = "http://javahtmlproxy.local/";
const urlTargetNode = "http://nodehtmlproxy.local/";

//Mensajes
const strMsg = {
    "wait": "Espere por favor",
    "load": "Cargando...",
    "time": "Tiempo de espera superado",
    "out": "Error en conexión"
};

//Body para el index
const objInit = {
    "menu": "Articulos "
}