//Funciones basicas
var strHost = window.location.href;
if (strHost.indexOf("php") > 0) {
    urlTarget = urlTargetPhp;
} else if (strHost.indexOf("java") > 0) {
    urlTarget = urlTargetJava;
}

const fncPage = function(text) {
    document.getElementById('contenido-titulo').innerHTML = text["data"][0]["txt_title"];
    document.getElementById('contenido-cuerpo').innerHTML = text["data"][0]["txt_article"];
};

const fncMenuExt = function(text) {
    strCorp = '';
    text["data"].forEach(element => {
        strCorp += '<li class="nav-item active"><a class="nav-link"  href="#" onclick="javascript:sendPage(' + "'" + element["str_link"] + "'" + ')">' + element["str_link"] + ' <span class="sr-only">(current)</span></a></li>';
    });
    document.getElementById('contenido-menu').innerHTML = strCorp;
}

function sendPage(page) {
    AjaxCall(urlTarget + "outFunctions/" + page, fncPage, "GET", "");
}

function fncCargarLogin(text) {
    document.getElementById("locateLogin").innerHTML = text;
    document.getElementById("btnAccess").addEventListener("click", function(event) {
        event.preventDefault()
    });
}

function formAccess() {
    objAccess = {
        "user": document.getElementById("inputUser").value,
        "pass": document.getElementById("inputPass").value
    };
    /*{
        "user": "admin",
        "pass": "admin"
    }*/
    AjaxCall(urlTarget + "users/", fncAccess, "POST", JSON.stringify(objAccess));
}

function fncAccess(text) {
    if ((text["data"][0]["big_id"] > 0) && (text["jwt"] != null)) {
        document.getElementById("welcome").style.display = "none";
        document.getElementById("program").style.display = "";
        var header = new Headers();
        header.append("jwt", text["jwt"]);
        fncConstruc
        document.getElementById("jwt").value = text["jwt"];
        var menu = text["menu"]["menus"];
        var struc = "";
        for (var i = 0; i < menu.length; i++) {
            struc += '<li>';
            struc += '<a>';
            struc += menu[i]["str_menu"] + '</a>';
            var submenu = menu[i]["str_submenu"];
            if (submenu.length > 0) {
                struc += '<ul>';
                for (var h = 0; h < submenu.length; h++) {
                    struc += '<li>';
                    if (submenu[h]["str_menu"] == "-") {
                        struc += '<a>-------------------------</a>';
                    } else {
                        if (((submenu[h]["str_shortcut"] != "") && (submenu[h]["str_shortcut"] != null)) && (submenu[h]["str_shortcut"] != "null")) {
                            struc += '<a id="' + submenu[h]["big_id"] + '" href="#" onclick="fncActionMenu(' + "'" + submenu[h]["str_action"] + "'" + ')"><span style="display: inline-block;">' + submenu[h]["str_menu"] + '</span><span style="display: inline-block; float: right;">' + submenu[h]["str_shortcut"] + '</span></a>';
                        } else {
                            struc += '<a id="' + submenu[h]["big_id"] + '" href="#" onclick="fncActionMenu(' + "'" + submenu[h]["str_action"] + "'" + ')">' + submenu[h]["str_menu"] + '</a>';
                        }
                    }
                }
                struc += '</ul>';
            }
            struc += '</li>';
        }
        if (text['dictionary'] != null) {
            for (var i = 0; i < text['dictionary'].length; i++) {
                sessionStorage.setItem(text['dictionary'][i]["str_name"], text['dictionary'][i]["str_value"]);
            }
        }
        document.getElementById("program").style.border = "thin solid #AAAAAA";
        document.getElementById("menu").innerHTML = struc;
    }
}

//Ejecuta la acción correspondiente para construcción de la pantalla
function fncActionMenu(action) {
    if (action == 'exit') {
        document.getElementById('jwt').value = '';
        window.location.replace('index.html');
    } else if (action == 'help') {
        AjaxCall(urlTarget + "component/help.html", fncHelp, "GET", "");
    } else if (action == 'about') {
        AjaxCall(urlTarget + "component/about.html", fncHelp, "GET", "");
    } else {
        AjaxCall(urlTarget + "functions/" + action, fncConstruc, "GET", "", "list", action);
    }
}
//Contrulle la pantalla en base al texto
function fncConstruc(text, target, action) {
    document.getElementById("jwt").value = text["jwt"];
    if (target == 'edit') {
        struc = '<div class="row"><div class="col-1"></div><div class="col-10">';
        if (translate(text['table'])) {
            struc += '<h2 class="text-center">' + translate(text['table']) + '</h2>';
        } else {
            struc += '<h2 class="text-center">' + text['table'] + '</h2>';
        }
        // Tenemos un solo resultado, asi que es una forma
        arrBody = text['data'][0];
        struc += '<form id="formData" name="formData">';
        for (var key in arrBody) {
            if (translate(key)) {
                title = translate(key);
            } else {
                title = key;
            }
            disabled = "";
            step = '';
            switch (key.substring(0, 3)) {
                case 'int':
                case 'big':
                    if (key.indexOf('_id') > 0) {
                        type = "numeric";
                        disabled = "disabled";
                    } else {
                        type = "dropbox";
                        switch (key.substring(4, 7)) {
                            case 'pro':
                                targetGet = 'profiles';
                                break;
                            case 'usr':
                                targetGet = 'users';
                                break;
                            case 'mop':
                                targetGet = 'menu_options';
                                break;
                            case 'men':
                                targetGet = 'menus';
                                break;
                            case 'par':
                                targetGet = 'parameters';
                                break;
                            case 'pre':
                                targetGet = 'previews';
                                break;
                        }
                    }
                    break;
                case 'bol':
                    type = "checkbox";
                    break;
                case 'txt':
                    type = "textarea";
                    break;
                case 'str':
                    type = "text";
                    break;
                case 'dat':
                    type = "date";
                    break;
                case 'num':
                    type = "numeric";
                    step = 'step="0.1"';
                    break;
            }
            struc += '<div class="form-group">';
            struc += '<label for="' + key + '">' + title + '</label>';
            if (type == 'textarea') {
                struc += '<textarea class="form-control" id="' + key + '" name="' + key + '">' + arrBody[key] + '</textarea>';
            } else if (type == 'dropbox') {
                //Verificar el nombre que le coloca al listado
                struc += '<div id="' + key + '_drop' + '"></div>';
                sessionStorage.setItem("idInsert", key + '_drop');
                AjaxCall(urlTarget + "functions/" + targetGet, fncDropBox, "GET", "", "", action);
            } else if (type == 'checkbox') {
                if (arrBody[key] == 1) {
                    checked = 'checked';
                } else {
                    checked = '';
                }
                struc += '<input type="' + type + '" class="form-control" id="' + key + '" name="' + key + '" ' + disabled + ' ' + checked + '/>';
            } else {
                struc += '<input type="' + type + '" class="form-control" id="' + key + '" name="' + key + '" placeholder="' + title + '" ' + disabled + ' value="' + arrBody[key] + '"></input>';
            }
            struc += '</div>';
        }
        struc += '</form></div><div class="col-1">';
        struc += '<button class="btn btn-info" onclick="javascript:fncPrevent(event); fncSave(' + "'" + text['table'] + "'" + ', ' + "'" + target + "'" + ');">Guardar</button>';
        struc += '</div>';
    } else if (target == "add") {
        struc = '<div class="row"><div class="col-1"></div><div class="col-10">';
        if (translate(text['table'])) {
            struc += '<h2 class="text-center">' + translate(text['table']) + '</h2>';
        } else {
            struc += '<h2 class="text-center">' + text['table'] + '</h2>';
        }
        // Tenemos un solo resultado, asi que es una forma
        arrBody = text['data'];
        struc += '<form id="formData" name="formData">';
        pref = '';
        for (var i = 0; i < arrBody.length; i++) {
            if (translate(arrBody[i]['str_column'])) {
                title = translate(arrBody[i]['str_column']);
            } else {
                title = arrBody[i]['str_column'];
            }
            if (arrBody[i]['str_type'] == 'character varying') {
                type = "input";
                step = '';
                pref = 'str';
            } else if ((arrBody[i]['str_type'] == 'bigint') || (arrBody[i]['str_type'] == 'integer')) {
                if (arrBody[i]['str_column'].indexOf('_id') > 0) {
                    type = 'dropbox';
                    step = '';
                    pref = 'int';
                    switch (arrBody[i]['str_column'].substring(0, arrBody[i]['str_column'].indexOf('id') - 1)) {
                        case 'pro':
                            targetGet = 'profiles';
                            break;
                        case 'usr':
                            targetGet = 'users';
                            break;
                        case 'mop':
                            targetGet = 'menu_options';
                            break;
                        case 'men':
                            targetGet = 'menus';
                            break;
                        case 'par':
                            targetGet = 'parameters';
                            break;
                        case 'pre':
                            targetGet = 'previews';
                            break;
                    }
                } else {
                    type = 'number';
                    step = 'step="1"';
                    pref = 'int';
                }
            } else if (arrBody[i]['str_type'] == 'text') {
                type = 'textarea';
                step = '';
                pref = 'txt';
            } else if ((arrBody[i]['str_type'] == 'boolean') || (arrBody[i]['str_type'] == 'smallint')) {
                type = 'checkbox';
                step = '';
                pref = 'bol';
            } else if (arrBody[i]['str_type'] == 'date') {
                type = 'date';
                step = '';
                pref = 'dat';
            } else if (arrBody[i]['str_type'] == 'numeric') {
                type = 'numeric';
                step = 'step="0.1"';
                pref = 'num';
            }
            if (arrBody[i]['str_column'].indexOf('_id') > 0) {
                name = pref + "_" + arrBody[i]['str_column'].substring(0, arrBody[i]['str_column'].indexOf('_'));
            } else {
                name = pref + arrBody[i]['str_column'].substring(arrBody[i]['str_column'].indexOf('_'), arrBody[i]['str_column'].length);
            }
            if (i == 0) {
                if (target == 'edit') {
                    struc += '<div class="form-group">';
                    struc += '<label for="' + name + '">' + title + '</label>';
                    struc += '<input type="' + type + '" class="form-control" id="' + name + '" name="' + name + '" placeholder="' + title + '" disabled></input>';
                }
            } else {
                struc += '<div class="form-group">';
                struc += '<label for="' + name + '">' + title + '</label>';
                if (type == 'textarea') {
                    struc += '<textarea class="form-control" id="' + name + '" name="' + name + '"></textarea>';
                } else if (type == 'dropbox') {
                    //Verificar el nombre que le coloca al listado
                    struc += '<div id="' + name + '_drop' + '"></div>';
                    sessionStorage.setItem("idInsert", name + '_drop');
                    AjaxCall(urlTarget + "functions/" + targetGet, fncDropBox, "GET", "", "", action);
                } else if (type == 'checkbox') {
                    struc += '<input type="' + type + '" class="form-control" id="' + name + '" name="' + name + '">';
                } else {
                    struc += '<input type="' + type + '" class="form-control" id="' + name + '" name="' + name + '" placeholder="' + title + '"></input>';
                }
                struc += '</div>';
            }
        }
        struc += '</form></div><div class="col-1">';
        struc += '<button class="btn btn-info" onclick="javascript:fncPrevent(event); fncSave(' + "'" + text['table'] + "'" + ', ' + "'" + target + "'" + ');">Guardar</button>';
        struc += '</div>';
        if (target == 'edit') {
            sessionStorage.setItem("struct", arrBody);
            id = sessionStorage.getItem("id");
            AjaxCall(urlTarget + "functions/" + action + 's' + "/" + id, fncCharge, "GET", "", "edit", action);
        }
    } else if (target == "list") {
        // Tenemos varios registros así que se arma una tabla
        struc = '<div class="row"><div class="col-11">';
        arrHeader = text.data[0];
        arrBody = text.data;
        if (translate(text['table'])) {
            struc += '<h2 class="text-center">' + translate(text['table']) + '</h2>';
        } else {
            struc += '<h2 class="text-center">' + text['table'] + '</h2>';
        }
        struc += '<table class="table table-bordered" style="z-index: -999;" id="dataList"><thead class="thead-dark"><tr>';
        struc += '<th scope="col"> </th>';
        for (var key in arrHeader) {
            if (translate(key)) {
                title = translate(key);
            } else {
                title = key.substring(4, key.length);
                title.replace(/_/g, ' ');
                title = title.charAt(0).toUpperCase() + title.slice(1);
            }
            struc += '<th scope="col">' + title + '</th>';
        }
        struc += '</tr></thead><tbody>';
        for (var i = 0; i < arrBody.length; i++) {
            struc += '<tr>';
            for (var key in arrHeader) {
                if (key == 'big_id') {
                    //Colocar aquí el enlace para editar
                    struc += '<th scope="row"><input type="checkbox" name="chkIdDel(' + arrBody[i][key] + ')" id="chkIdDel(' + arrBody[i][key] + ')"/></th>';
                    struc += '<th><a href="#" onclick="javascript:fncEdit(' + arrBody[i][key] + ', ' + "'" + text['table'] + "'" + ')">' + arrBody[i][key] + '</a></th>';
                } else {
                    if (key.substring(0, 3) == 'bol') {
                        if (arrBody[i][key] == 1) {
                            struc += '<td>Si</td>';
                        } else {
                            struc += '<td>No</td>';
                        }
                    } else {
                        struc += '<td>' + arrBody[i][key] + '</td>';
                    }
                }
            }
            struc += '</tr>';
        }
        struc += '</tbody></table>';
        struc += '</div><div class="col-1">';
        struc += '<button class="btn btn-info" onclick="javascript:fncPrevent(event); fncAdd(' + "'" + text['table'] + "'" + ');">Agregar</button>';
        struc += '<button class="btn btn-danger" onclick="javascript:fncPrevent(event); fncDel(' + "'" + text['table'] + "'" + ');">Eliminar</button>';
        struc += '</div>';
    }
    struc += '</div></div>';
    document.getElementById('operation').innerHTML = struc;
}

function fncHelp(text) {
    document.getElementById('operation').innerHTML = text;
}

function translate(value) {
    if (sessionStorage.getItem(value) != null) {
        return sessionStorage.getItem(value);
    } else {
        return null;
    }
}

function fncAdd(action) {
    AjaxCall(urlTarget + "struct/" + action, fncConstruc, "GET", "", "add", action);
}

function fncEdit(id, action) {
    sessionStorage.setItem("id", id);
    AjaxCall(urlTarget + "functions/" + action + "s/" + id, fncConstruc, "GET", "", "edit", action);
    //AjaxCall(urlTarget + "struct/" + action, fncConstruc, "GET", "", "edit", action);
}

function fncDel(table) {
    var arrData = {};
    var strData = "";
    var separ = "";
    target = table + 's';
    arrData['menu'] = target;
    h = 0;
    for (var i = 0; i < document.getElementById('dataList').children[1].childNodes.length; i++) {
        if (document.getElementById('dataList').children[1].childNodes[i].cells[0].children[0].type == 'checkbox') {
            if (document.getElementById('dataList').children[1].childNodes[i].cells[0].children[0].checked) {
                if (h > 0) {
                    separ = ",";
                }
                txt = document.getElementById('dataList').children[1].childNodes[i].cells[0].children[0].id;
                var numb = txt.match(/\d/g);
                numb = numb.join("");
                strData += separ + numb;
                h += 1;
            }
        }
    }
    arrData['delete'] = strData;
    var json = JSON.stringify(arrData);
    AjaxCall(urlTarget + "functions/" + target, "", "DELETE", json, "list", "");
}

function fncSave(table, action) {
    var arrData = {};
    target = table + 's';
    arrData['menu'] = target;
    for (var i = 0; i < document.getElementById('formData').elements.length; i++) {
        switch (document.getElementById('formData').elements[i].type) {
            case 'textarea':
            case 'text':
                arrData[document.getElementById('formData').elements[i].id] = document.getElementById('formData').elements[i].value;
                break;
            case 'select':
            case 'select-one':
                arrData[document.getElementById('formData').elements[i].id] = document.getElementById('formData').elements[i].value;
                break;
            case 'checkbox':
                if (document.getElementById('formData').elements[i].checked) {
                    value = 1;
                } else {
                    value = 0;
                }
                arrData[document.getElementById('formData').elements[i].id] = value;
                break;
            case 'radiobutton':
                if (document.getElementById('formData').elements[i].checked) {
                    value = 1;
                } else {
                    value = 0;
                }
                arrData[document.getElementById('formData').elements[i].id] = value;
                break;
        }
    }
    var json = JSON.stringify(arrData);
    if (action == 'edit') {
        AjaxCall(urlTarget + "functions/" + target, "", "PUT", json, "list", action);
    } else if (action == 'add') {
        AjaxCall(urlTarget + "functions/" + target, "", "POST", json, "list", action);
    }
}

function fncDropBox(data) {
    idInsert = sessionStorage.getItem("idInsert");
    idObject = idInsert.substring(0, idInsert.indexOf('_drop'));
    struc = '<select class="custom-select" id="' + idObject + '" name="' + idObject + '">';
    for (var i = 0; i < data['data'].length; i++) {
        struc += '<option value="' + data['data'][i]['big_id'] + '">' + data['data'][i]['str_name'] + '</option>';
    }
    struc += '</select>';
    document.getElementById(idInsert).innerHTML = struc;
}

function fncCharge(data) {
    arrBody = sessionStorage.getItem("struct");
    arrData = data['data'][0];
    for (var key in arrData) {
        try {
            //Cambiar el nombre del campo id de usr
            //Ver como lograr que se arregle la ventana de preview (ademas buscar forma de traducción automatica)
            if (document.getElementById(key).tagName == 'input') {
                document.getElementById(key).value = arrData[key];
            } else if (document.getElementById(key).tagName == 'select') {

            } else if (document.getElementById(key).tagName == 'checkbox') {

            } else if (document.getElementById(key).tagName == 'radiobutton') {

            }
        } catch {
            console.log(console.error());
        }
    }
}

function fncPrevent(event) {
    event.preventDefault();
}